#!/bin/bash

rm -R dist/*
mkdir -p dist/images
cp index.html dist
cp Ostwache-Skizze.pdf dist
cp images/*.jpg dist/images
cp images/*.png dist/images
mogrify -format jpg -quality 80 -resize 1600x dist/images/*.jpg
mogrify -format jpg -quality 80 -resize 1600x dist/images/*.png
rm dist/images/*.png
